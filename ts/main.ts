let oldWords: string[] = [
    'hola',
    'y',
    'el',
    'la'
];
let newWords: string[] = [];
let usedOldWords: string[] = [];
let maxNewWords = 3;

let story = document.getElementById('story') as HTMLTextAreaElement;
let oldList = document.getElementById('oldList');
let newList = document.getElementById('newList');

async function load() {
    await fetch('http://localhost:3000/load')
    .then(response => response.json())
    .then(data => {
        oldWords = data.oldWords;
        story.value = data.story;
        maxNewWords = data.maxNewWords;
    });
}

function save() {
    const saveData = {
        oldWords,
        story: story.value,
        maxNewWords,
    };
    fetch('http://localhost:3000/save', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(saveData),
    })
}

function formatText(str: string): string {
    return str.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g," ").toLowerCase();
}

function refreshLists(){
    oldList.innerHTML = "";
    newList.innerHTML = "";
    for(let i = 0; i < oldWords.length; i++){
        let editButton = document.createElement('button');
        editButton.innerText = 'edit';
        editButton.onclick = function() {
            let index = parseInt(deleteButton.parentElement.getAttribute('index'));
            let li = document.querySelector('li[index="' + index + '"]');
            li.innerHTML = '';

            let textBox = document.createElement('input');
            textBox.type = 'text';
            textBox.value = oldWords[index];
            textBox.size = 8;
            textBox.onkeydown = event => {
                if (event.key === 'Enter'){
                    oldWords[index] = formatText(textBox.value).replace(/ /g, '');
                    processStoryValue();
                }
            };
            li.appendChild(textBox);
            textBox.focus();
        };

        let deleteButton = document.createElement('button');
        deleteButton.innerText = 'del';
        deleteButton.onclick = function() {
            oldWords.splice(parseInt(deleteButton.parentElement.getAttribute('index')), 1);
            processStoryValue();
        };

        let text = document.createElement('span');
        text.innerText = oldWords[i];

        let el = document.createElement('li');
        el.setAttribute('index', i.toString());
        el.appendChild(text);
        el.innerText += ' ';
        el.appendChild(editButton);
        el.appendChild(deleteButton);
        if (usedOldWords.includes(oldWords[i])) {
            el.style.color = 'green';
        }
        oldList.appendChild(el);
    }

    let addLi = document.createElement('li');
    let addButton = document.createElement('button');
    addButton.innerText = 'add';
    addButton.onclick = function() {
        addLi.innerHTML = '';
        let textBox = document.createElement('input');
        textBox.type = 'text';
        textBox.size = 8;
        textBox.onkeydown = event => {
            if (event.key === 'Enter'){
                oldWords.push(formatText(textBox.value).replace(/ /g, ''));
                processStoryValue();
            }
        };
        addLi.appendChild(textBox);
        textBox.focus();
    };
    addLi.appendChild(addButton);
    oldList.appendChild(addLi);

    for(let i = 0; i < newWords.length; i++){
        let el = document.createElement('li');
        el.innerText = newWords[i];
        if (i >= maxNewWords) {
            el.style.color = 'red';
        }
        newList.appendChild(el);
    }
}

function processStoryValue() {
    let lastChar = story.value.substring(story.value.length - 1, story.value.length);
    if (lastChar === ' ' || lastChar === '.' || story.value.length === 0) {
        let words: string[] = formatText(story.value)
        .split(' ')
        .filter(el => {
            return el !== '';
        });
        words = Array.from(new Set(words));
        newWords = words.filter(word => {
            return !oldWords.includes(word);
        });
        usedOldWords = words.filter(word => {
            return oldWords.includes(word);
        });

        save();
        refreshLists();
    }
}

story.onkeyup = processStoryValue;

async function main() {
    await load();
    processStoryValue();
    refreshLists();
}

main();