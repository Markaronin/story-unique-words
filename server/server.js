const express = require('express');
const fs = require('fs');
const path = require('path');

const port = 3000;
const app = express();
const dir = path.resolve(__dirname, '..');
console.log(dir);

app.use(express.static(dir));
app.use(express.json());

app.get('/load', function (req, res) {
    fs.readFile(dir + '/data.json', (err, data) => {
        res.send(data);
    });
});
app.post('/save', function (req, res) {
    let data = req.body;
    fs.writeFile(dir + '/data.json', JSON.stringify(data), () => {
        res.status(200).send();
    });
});

app.listen(port, function () {
    console.log("Server is running on "+ port +" port");
});
